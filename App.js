import {createNativeStackNavigator} from "react-native-screens/native-stack";
import { NavigationContainer } from '@react-navigation/native';
import HomeScreen from "./screen/HomeScreen";
import {Component} from "react";
import TestScreen from "./screen/TestScreen";

const Stack = createNativeStackNavigator();

export default class App extends Component{

  render() {
    return (
        <NavigationContainer>
          <Stack.Navigator>
            <Stack.Screen name="home" component={HomeScreen} />
            <Stack.Screen name="test" component={TestScreen} />
          </Stack.Navigator>
        </NavigationContainer>
    );
  }
}
