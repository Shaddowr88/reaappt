import React from "react";
import {Button, Text, View} from "react-native";

export default function HomeScreen ({navigation}) {
        return(
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <Text>
                    Hello Home Screen
                </Text>

                <Button
                    title="Test"
                onPress={() => navigation.navigate('test')}
                />
            </View>

        )
}


