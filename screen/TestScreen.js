import React from "react";
import {Button, Text, View} from "react-native";


export default function TestScreen({navigation}) {
        return(
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <Text>
                    Hello Home Screen
                </Text>

                <Button title="Home"
                        onPress={() => navigation.navigate('home')}
                />
            </View>

        )

}
